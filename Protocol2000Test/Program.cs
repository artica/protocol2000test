﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace Protocol2000Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
                {
                    using (TcpClient avSwitch = new TcpClient("192.168.1.39", 5000))
                    {
                            // read a bit / binary string, convert to byte buffer and send in bytes
                            Stream stream = avSwitch.GetStream();
                            //string input = "00000001100000011000000110000001"; // 1 129 129 129
                            string input = "00000001100000101000000110000001"; // 1 130 129 129
                            int numOfBytes = input.Length / 8;
                            byte[] bytes = new byte[numOfBytes];
                            for(int i = 0; i < numOfBytes; ++i)
                            {
                                bytes[i] = Convert.ToByte(input.Substring(8 * i, 8), 2);
                            }
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                    }
                }
                catch (SocketException ex)
                {
                    Console.Out.WriteLine("Unable to connect :" + ex.Message);
                }
        }
    }
}
